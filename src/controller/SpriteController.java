package controller;

import model.basics.Sprite;
import model.basics.BaseCharacter;
import utils.Constants;
import utils.Res;
import utils.Utils;

import java.awt.*;

/**
 * SpriteController: it handles the movements.
 * Created by sapi9 on 12/03/2017.
 */
public abstract class SpriteController {

    public SpriteController(){
    }

    /**
     * Little template method with the subclasses
     * @param character principal character
     * @param obstacle obstacle
     */
    public void checkContact(BaseCharacter character, Sprite obstacle){
        if (this.isNearby(character,obstacle))
            this.contact(character,obstacle);
    }

    public abstract void contact(BaseCharacter character, Sprite obstacle);

    public Image walk(BaseCharacter character, String name, int frequency) {
        String str = Res.IMG_BASE + name + (!character.isMoving() || ++character.counter % frequency == 0 ?
                Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (character.isToRight() ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT;
        return Utils.getImage(str);
    }

    public static void move(BaseCharacter character) {
        character.setOffset(character.isToRight() ? Constants.SX : Constants.DX);
        character.getCoordinates().setX(character.getCoordinates().getX() + character.getOffset());

    }
    public boolean hitAhead(Sprite mainEl, Sprite el) {
            return !(mainEl.getX() + mainEl.getWidth() < el.getX() || mainEl.getX() + mainEl.getWidth() > el.getX() + Constants.DELTA_HIT ||
                    mainEl.getY() + mainEl.getHeight() <= el.getY() || mainEl.getY() >= el.getY() + el.getHeight());
    }

    public boolean hitBack(Sprite mainEl, Sprite el) {
        return !(mainEl.getX() > el.getX() + el.getWidth() || mainEl.getX() + mainEl.getWidth() < el.getX() + el.getWidth() - Constants.DELTA_HIT ||
                mainEl.getY() + mainEl.getHeight() <= el.getY() || mainEl.getY()>= el.getY() + el.getHeight());
    }

    public boolean hitBelow(Sprite mainEl, Sprite og) {
        return !(mainEl.getX() + mainEl.getWidth() < og.getX() + Constants.DELTA_HIT || mainEl.getX() > og.getX() + og.getWidth() - Constants.DELTA_HIT ||
                mainEl.getY() + mainEl.getHeight() < og.getY() || mainEl.getY() + mainEl.getHeight() > og.getY() + Constants.DELTA_HIT);
    }

    public boolean hitAbove(Sprite mainEl, Sprite og) {
        return !(mainEl.getX() + og.getWidth() < og.getX() + Constants.DELTA_HIT || mainEl.getX() > og.getX() + og.getWidth() - Constants.DELTA_HIT ||
                mainEl.getY() < og.getY() + og.getHeight() || mainEl.getY() > og.getY() + og.getHeight() + Constants.DELTA_HIT);
    }

    public boolean isNearby(Sprite mainEl, Sprite el) {
        if ((mainEl.getX() > el.getX() - Constants.PROXIMITY_MARGIN && mainEl.getX() < el.getX() + el.getWidth() + Constants.PROXIMITY_MARGIN)
                || (mainEl.getX() + mainEl.getWidth() > el.getX() - Constants.PROXIMITY_MARGIN && mainEl.getX() + mainEl.getWidth() < el.getX() + el.getWidth() + Constants.PROXIMITY_MARGIN))
            return true;
        return false;
    }

}
