package controller;

import model.Mushroom;
import model.Turtle;
import model.basics.Sprite;
import model.basics.BaseCharacter;
import model.basics.Coordinates;
import utils.Constants;

/**
 * EnemiesController.
 * It extends SpriteController, as it handles the characters of Mushroom and Turtle.
 * Created by sapi9 on 14/03/2017.
 */
public class EnemiesController extends SpriteController {


    private Turtle turtle;
    private Mushroom mushroom;

    public EnemiesController(){
        this.mushroom=new Mushroom(new Coordinates(Constants.MUSH_X, Constants.MUSH_Y));
        this.turtle = new Turtle(new Coordinates(Constants.TURTLE_X, Constants.TURTLE_Y));
    }

    public Mushroom getMushroom(){
        return this.mushroom;
    }
    public Turtle getTurtle(){
        return this.turtle;
    }

    /**
     * Method inherited by the abstract SpriteController,
     * as part of the template method of checkContact.
     * It handles the contact of the enemy with an obstacle.
     * @param character enemy
     * @param obstacle block
     */
    public void contact(BaseCharacter character, Sprite obstacle) {

        if (this.hitAhead(character, obstacle) && character.isToRight()) {
            character.setToRight(false);
            character.setOffset(-1);
        } else if (this.hitBack(character, obstacle) && !character.isToRight()) {
            character.setToRight(true);
            character.setOffset(1);
        }
    }

}
