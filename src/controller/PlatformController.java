package controller;

import game.Audio;
import model.basics.Score;
import view.Platform;
import model.Mario;
import model.objects.Block;
import model.objects.Coin;
import model.objects.GameObject;
import model.objects.Tunnel;
import utils.Constants;
import utils.Res;

import java.awt.*;
import java.util.ArrayList;

/**
 * PlatformController: class that manages the logic of all of the elements of the platform
 * Created by sapi9 on 14/03/2017.
 */
public class PlatformController {

    private static PlatformController instance = null;

    private MarioController marioController;
    private EnemiesController enemiesController;

    private Platform platform;

    private ArrayList<GameObject> objects;

    private int backgroundPosX;
    private int mov;
    private int xPosition;
    private int floorOffsetY;
    private int heightLimit;

    //Observable object
    private Score score;

    private PlatformController() { }

    /**
     * Instance of the singleton,
     * in order to get the same instance of Platform, Mario and Enemies from everywhere
     * @return instance
     */
    public static PlatformController getInstance() {
        if(instance == null) {
            instance = new PlatformController();

        }
        return instance;
    }


    private void loadPlatform(){

        this.backgroundPosX = -50;
        this.mov = 0;
        this.xPosition = -1;
        this.floorOffsetY = 293;
        this.heightLimit = 0;
        this.marioController = new MarioController();
        this.enemiesController = new EnemiesController();

        this.objects = new ArrayList<>();

        for(int i = 0; i< Constants.BLOCKS_NUM; i++){
            this.objects.add(new Block(Constants.BLOCKS_COORDINATES[i]));
        }
        for (int i = 0; i<Constants.TUNNELS_NUM; i++){
            this.objects.add(new Tunnel(Constants.TUNNEL_COORDINATES[i]));
        }
        for (int i = 0; i<Constants.PIECES_NUM; i++){
            this.objects.add(new Coin(Constants.PIECES_COORDINATES[i]));
        }

        this.platform = new Platform();
        this.score = new Score();
        score.addObserver(this.platform);
    }


    /**
     * Paint method, called by Swing inside Platform
     */
    public void paint(Graphics g, Image imgBackground, Image castle, Image start, Image imgFlag, Image imgCastle){
        this.handleContactWithObjects();
        // Moving fixed model.objects
        this.updateWithWalking(g, imgBackground, castle, start, imgFlag, imgCastle);

        this.handleMarioJumping(g);

        this.handleEnemiesLife(g);

    }

    private void handleContactWithObjects(){
        for (int i = 0; i < objects.size(); i++) {

            this.marioController.checkContact(this.marioController.getMario(),this.objects.get(i));
            this.enemiesController.checkContact(this.enemiesController.getMushroom(),this.objects.get(i));
            this.enemiesController.checkContact(this.enemiesController.getTurtle(),this.objects.get(i));

            if(this.objects.get(i) instanceof Coin){
                if (this.marioController.contactCoin(this.objects.get(i))) {
                    Audio.playSound(Res.AUDIO_MONEY);
                    this.objects.remove(i);
                    //Change data for Observer pattern
                    this.score.changeData(this.score.incrementScore());
                }
            }
        }

        this.enemiesController.checkContact(this.enemiesController.getMushroom(), enemiesController.getTurtle());
        this.enemiesController.checkContact(this.enemiesController.getTurtle(), enemiesController.getMushroom());
        this.marioController.checkContact(this.marioController.getMario(),this.enemiesController.getMushroom());
        this.marioController.checkContact(this.marioController.getMario(),this.enemiesController.getTurtle());
    }

    private void handleMarioJumping(Graphics g){
        if (this.marioController.getMario().isJumping())
            g.drawImage(this.marioController.doJump(), this.marioController.getMario().getX(), this.marioController.getMario().getY(), null);
        else
            g.drawImage(this.marioController.walk(this.marioController.getMario(),Res.IMGP_CHARACTER_MARIO, Constants.MARIO_FREQUENCY), this.marioController.getMario().getX(), this.marioController.getMario().getY(), null);

    }

    private void handleEnemiesLife(Graphics g){
        if (this.enemiesController.getMushroom().isAlive())
            g.drawImage(this.enemiesController.walk(this.enemiesController.getMushroom(),
                    Res.IMGP_CHARACTER_MUSHROOM, Constants.MUSHROOM_FREQUENCY), this.enemiesController.getMushroom().getX(), this.enemiesController.getMushroom().getY(), null);
        else
            g.drawImage(this.enemiesController.getMushroom().deadImage(), this.enemiesController.getMushroom().getX(), this.enemiesController.getMushroom().getY() + Constants.MUSHROOM_DEAD_OFFSET_Y, null);

        if (this.enemiesController.getTurtle().isAlive())
            g.drawImage(this.enemiesController.walk(enemiesController.getTurtle(),Res.IMGP_CHARACTER_TURTLE, Constants.TURTLE_FREQUENCY), this.enemiesController.getTurtle().getX(), this.enemiesController.getTurtle().getY(), null);
        else
            g.drawImage(this.enemiesController.getTurtle().deadImage(), this.enemiesController.getTurtle().getX(), this.enemiesController.getTurtle().getY() + Constants.TURTLE_DEAD_OFFSET_Y, null);

    }

    private void updateWithWalking(Graphics g, Image imageBackground, Image castle, Image start, Image imgFlag, Image imgCastle){
        this.updateBackgroundOnMovement();

        g.drawImage(imageBackground, this.backgroundPosX, 0, null);
        g.drawImage(castle, 10 - this.xPosition, 95, null);
        g.drawImage(start, 220 - this.xPosition, 234, null);

        for (GameObject obj : this.objects) {
            g.drawImage(obj.getImgObj(), obj.getX(), obj.getY(), null);

            if(obj instanceof Coin){
                g.drawImage(obj.imageOnMovement(), obj.getX(), obj.getY(), null);
            }

            if (this.xPosition >= 0 && this.xPosition <= 4600) {
                obj.move();
            }
        }
        SpriteController.move(this.enemiesController.getMushroom());
        SpriteController.move(this.enemiesController.getTurtle());

        g.drawImage(imgFlag, Constants.FLAG_X_POS - this.xPosition, Constants.FLAG_Y_POS, null);
        g.drawImage(imgCastle, Constants.CASTLE_X_POS - this.xPosition, Constants.CASTLE_Y_POS, null);
    }

    private void updateBackgroundOnMovement() {
        if (this.xPosition >= 0 && this.xPosition <= 4600) {
            this.xPosition = this.xPosition + this.mov;
            // Moving the screen to give the impression that Mario is walking
            this.backgroundPosX = this.backgroundPosX - this.mov;
        }

        if (this.backgroundPosX == -800) {
            this.backgroundPosX = 0;
        }
        else if (this.backgroundPosX == 0) {
            //Moving left
            this.backgroundPosX = -800;
        }
    }

    /**
     * Get Mario from the MarioController
     * @return Mario
     */
    public Mario getMario(){
        return this.marioController.getMario();
    }

    /**
     * Get the current instance of the Platform
     * @return Platform
     */
    public Platform getPlatform(){
        if(this.platform==null){
            this.loadPlatform();
        }
        return this.platform;
    }


    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMov() {
        return mov;
    }

    public int getXPosition() {
        return xPosition;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setXPosition(int xPos) {
        this.xPosition = xPos;
    }

    public void setMov(int mov) {
        this.mov = mov;
    }

    public void setBackgroundPosX(int x) {
        this.backgroundPosX = x;
    }
}
