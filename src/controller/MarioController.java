package controller;

import model.Mario;
import model.basics.Sprite;
import model.basics.BaseCharacter;
import model.basics.Coordinates;
import utils.Constants;
import utils.Res;
import utils.Utils;

import java.awt.*;

/**
 * MarioController
 * It extends SpriteController, as it handles the characters of Mario.
 * Created by sapi9 on 11/03/2017.
 */
public class MarioController extends SpriteController {


    private Mario mario;

    public MarioController(){
        this.mario=new Mario(new Coordinates(Constants.MARIO_X, Constants.MARIO_Y));
    }

    public Mario getMario(){
        return this.mario;
    }


    /**
     * Method that allows Mario jumping
     * @return image
     */
    public Image doJump() {
        String str;

        this.mario.increaseJumping();

        if (this.mario.getJumpingExtent() < Constants.JUMPING_LIMIT) {
            if (this.mario.getY() > PlatformController.getInstance().getHeightLimit())
                this.mario.getCoordinates().setY(this.mario.getY() - 4);
            else
                this.mario.setJumpingExtent(Constants.JUMPING_LIMIT);

            str = this.mario.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else if (this.mario.getY() + this.mario.getHeight() <
                PlatformController.getInstance().getFloorOffsetY()) {
            this.mario.getCoordinates().setY(this.mario.getY() + 1);
            str = this.mario.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else {
            str = this.mario.isToRight() ? Res.IMG_MARIO_ACTIVE_DX : Res.IMG_MARIO_ACTIVE_SX;
            this.mario.setJumping(false);
            this.mario.setJumpingExtent(0);
        }

        return Utils.getImage(str);
    }

    /**
     * Method inherited by the abstract SpriteController,
     * as part of the template method of checkContact.
     * It handles the contact of Mario with obstacles.
     * @param character enemy
     * @param obstacle obstacle
     */
    public void contact(BaseCharacter character, Sprite obstacle) {
        if(obstacle instanceof BaseCharacter){
            this.contactEnemy((BaseCharacter)obstacle);
        } else {
            PlatformController platform = PlatformController.getInstance();

            if ((this.hitAhead(this.mario,obstacle) && this.mario.isToRight()) || (this.hitBack(mario,obstacle) && !this.mario.isToRight())) {

                platform.setMov(0);
                this.mario.setMoving(false);
            }

            if (this.hitBelow(mario,obstacle) && this.mario.isJumping()) {
                platform.setFloorOffsetY(obstacle.getY());
            } else if (!this.hitBelow(mario,obstacle)) {
                platform.setFloorOffsetY(Constants.FLOOR_OFFSET_Y_INITIAL);
                if (!this.mario.isJumping()) {
                    this.mario.getCoordinates().setY(Constants.MARIO_OFFSET_Y_INITIAL);
                }

                if (this.hitAbove(mario,obstacle)) {
                    platform.setHeightLimit(obstacle.getY() + obstacle.getHeight()); // the new sky goes below the object
                } else if (!this.hitAbove(mario,obstacle) && !this.mario.isJumping()) {
                    platform.setHeightLimit(0); // initial sky
                }
            }
        }

    }


    private void contactEnemy(BaseCharacter character) {
        if (this.hitAhead(this.mario, character) || this.hitBack(this.mario,character)) {
            if (character.isAlive()) {
                this.mario.setMoving(false);
                this.mario.setAlive(false);
            }
        } else if (this.hitBelow(mario, character)) {
            character.setMoving(false);
            character.setAlive(false);
        }
    }

    public boolean contactCoin(Sprite piece) {
        if (this.hitBack(mario,piece) || this.hitAbove(mario,piece) || this.hitAhead(mario,piece) || this.hitBelow(mario,piece)){
            return true;
        }

        return false;
    }

}
