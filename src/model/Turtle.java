package model;

import java.awt.Image;

import model.basics.AbstractEnemy;
import model.basics.Coordinates;
import utils.Constants;
import utils.Res;
import utils.Utils;

public class Turtle extends AbstractEnemy {

    public Turtle(Coordinates coordinates) {
        super(coordinates, Constants.TURTLE_WIDTH, Constants.TURTLE_HEIGHT);
    }

    public Image deadImage() {
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }
}
