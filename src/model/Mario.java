package model;

import model.basics.BaseCharacter;
import model.basics.Coordinates;
import model.basics.Dimensions;
import utils.Constants;

public class Mario extends BaseCharacter {

    private boolean jumping;
    private int jumpingExtent;

    public Mario(Coordinates coordinates) {
        super(coordinates, new Dimensions(Constants.MARIO_WIDTH, Constants.MARIO_HEIGHT));

        this.jumping = false;
        this.jumpingExtent = 0;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public int getJumpingExtent(){
        return this.jumpingExtent;
 }

    public void increaseJumping(){
     this.jumpingExtent++;
    }

    public void setJumpingExtent(int jumpingExtent){
        this.jumpingExtent = jumpingExtent;
    }

}
