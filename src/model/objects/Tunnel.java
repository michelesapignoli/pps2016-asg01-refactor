package model.objects;

import model.basics.Coordinates;
import model.basics.Dimensions;
import utils.Constants;
import utils.Res;
import utils.Utils;

import java.awt.*;

public class Tunnel extends GameObject {

    public Tunnel(Coordinates coordinates) {
        super(coordinates, new Dimensions(Constants.TUNNEL_WIDTH, Constants.TUNNEL_HEIGHT));
        super.imgObj = Utils.getImage(Res.IMG_TUNNEL);
    }

    @Override
    public Image imageOnMovement() {
        return null;
    }

}
