package model.objects;

import model.basics.Coordinates;
import model.basics.Dimensions;
import utils.Constants;
import utils.Res;
import utils.Utils;

import java.awt.*;

public class Block extends GameObject {


    public Block(Coordinates coordinates) {
        super(coordinates, new Dimensions(Constants.BLOCK_WIDTH, Constants.BLOCK_HEIGHT));
        super.imgObj = Utils.getImage(Res.IMG_BLOCK);
    }

    @Override
    public Image imageOnMovement() {
        return null;
    }
}
