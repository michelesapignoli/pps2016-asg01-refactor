package model.objects;

import java.awt.Image;

import controller.PlatformController;
import model.basics.Coordinates;
import model.basics.Sprite;
import model.basics.Dimensions;

public abstract class GameObject extends Sprite {

    protected Image imgObj;

    public GameObject(Coordinates coordinates, Dimensions dimensions) {
        super(coordinates, dimensions);
    }

    public abstract Image imageOnMovement();

    public int getX() {
        return this.coordinates.getX();
    }

    public int getY() {
        return this.coordinates.getY();
    }

    public Image getImgObj() {
        return imgObj;
    }

    public void setX(int x) {
        this.coordinates.setX(x);
    }

    public void move() {
        if (PlatformController.getInstance().getXPosition() >= 0) {
            this.setX(this.coordinates.getX() - PlatformController.getInstance().getMov());
        }
    }

}
