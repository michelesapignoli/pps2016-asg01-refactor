package model.basics;

/**
 * Sprite, with Dimensions and Coordinates
 */
public class Sprite {
	protected Dimensions dimensions;
	protected Coordinates coordinates;

	public Sprite(Coordinates coordinates, Dimensions dimensions){
		this.coordinates=coordinates;
		this.dimensions=dimensions;
	}

	public int getX(){
		return this.coordinates.getX();
	}
	public int getY(){
		return this.coordinates.getY();
	}
	public int getHeight(){
		return this.dimensions.getHeight();
	}
    public int getWidth(){
        return this.dimensions.getWidth();
    }
	public void setX(int x){
		this.coordinates.setX(x);
	}

	public Coordinates getCoordinates() {
		return this.coordinates;
	}


}
