package model.basics;

/**
 * Dimensions Class
 * Created by sapi9 on 11/03/2017.
 */
public class Dimensions {
    private int height;
    private int width;

    public Dimensions(int width, int height){
        this.width=width;
        this.height=height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
