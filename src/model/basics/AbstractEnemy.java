package model.basics;

import controller.SpriteController;
import utils.Constants;

import java.awt.*;

/**
 * AbstractEnemy. Turtle and Mushroom extends from this.
 * Created by sapi9 on 11/03/2017.
 */
public abstract class AbstractEnemy extends BaseCharacter implements Runnable{


    public AbstractEnemy(Coordinates coordinates,int  width, int height){
        super(coordinates, new Dimensions(width, height));
        this.setToRight(true);
        this.setMoving(true);
        this.setOffset(1);

        Thread movement = new Thread(this);
        movement.start();
    }

    @Override
    public void run() {
        while (true) {
            if (this.isAlive()) {
                SpriteController.move(this);
                try {
                    Thread.sleep(Constants.ENEMY_PAUSE);
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

    public abstract Image deadImage();
}
