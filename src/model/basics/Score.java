package model.basics;

import utils.Constants;

import java.util.Observable;

/**
 * Observable score. Part of Observer pattern used to refresh the score.
 * Created by sapi9 on 15/03/2017.
 */
public class Score extends Observable {
    private int score;

    public Score() {
        super();
    }

    /**
     * Metod called whenever a coin is gained by Mario.
     * @param score score
     */
   public void changeData(int score) {
        this.score = score;
        setChanged(); // the two methods of Observable class
        notifyObservers(score);
    }


    public int incrementScore(){
        this.score += Constants.SCORE_INCREMENT;
        return this.score;
    }
}
