package model.basics;

/**
 * BaseCharacter, extends Sprite
 */
public class BaseCharacter extends Sprite {


    private boolean moving;
    private boolean toRight;
    public int counter;
    private boolean alive;
    private int offset;

    public BaseCharacter(Coordinates coordinates, Dimensions dimensions) {
       super(coordinates, dimensions);
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
    }


    public boolean isAlive() {
        return alive;
    }


    public boolean isToRight() {
        return toRight;
    }

    public void setAlive(boolean isAlive) {
        this.alive = isAlive;
    }


    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    public boolean isMoving(){
        return this.moving;
    }


    public void setOffset(int offset){
        this.offset=offset;
    }

    public int getOffset(){
        return this.offset;
    }
}
