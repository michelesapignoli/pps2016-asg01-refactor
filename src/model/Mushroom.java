package model;

import java.awt.Image;

import model.basics.AbstractEnemy;
import model.basics.Coordinates;
import utils.Constants;
import utils.Res;
import utils.Utils;

public class Mushroom extends AbstractEnemy {

    public Mushroom(Coordinates coordinates) {
        super(coordinates, Constants.MUSH_WIDTH, Constants.MUSH_HEIGHT);
    }



    public Image deadImage() {
        return Utils.getImage(this.isToRight() ? Res.IMG_MUSHROOM_DEAD_DX : Res.IMG_MUSHROOM_DEAD_SX);
    }


}
