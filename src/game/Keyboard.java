package game;

import controller.PlatformController;
import model.Mario;
import utils.Constants;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static controller.PlatformController.*;

public class Keyboard implements KeyListener {
    private PlatformController platform;

    public Keyboard() {
        platform = getInstance();
    }

    @Override
    public void keyPressed(KeyEvent e) {

        Mario mario = platform.getMario();

        if (mario.isAlive()) {

            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                // per non fare muovere il castello e start
                if (platform.getXPosition() == Constants.INITIAL_POS_PLATFORM) {
                    platform.setXPosition(0);
                    platform.setBackgroundPosX(Constants.INITIAL_POS_BACKGROUND);
                }
                mario.setMoving(true);
                mario.setToRight(true);
                platform.setMov(Constants.SX); // si muove verso sinistra
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (platform.getXPosition() == Constants.FINAL_POS_PLATFORM + 1) {
                    platform.setXPosition(Constants.FINAL_POS_PLATFORM);
                    platform.setBackgroundPosX(Constants.INITIAL_POS_BACKGROUND);
                }

                mario.setMoving(true);
                mario.setToRight(false);
                platform.setMov(Constants.DX); // si muove verso destra
            }
            // salto
            if (e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode()==KeyEvent.VK_SPACE) {
                platform.getMario().setJumping(true);
                Audio.playSound("/resources/audio/jump.wav");
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        platform.getMario().setMoving(false);
        platform.setMov(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
