package game;

import controller.PlatformController;
import utils.Constants;
import view.Platform;

import javax.swing.*;

public class Main {


    public static void main(String[] args) {
        JFrame window = new JFrame(Constants.WINDOW_TITLE);
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.setSize(Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT);
        window.setLocationRelativeTo(null);
        window.setResizable(true);
        window.setAlwaysOnTop(true);

        Platform scene = PlatformController.getInstance().getPlatform();
        window.setContentPane(scene);
        window.setVisible(true);

        Thread timer = new Thread(new Refresh());
        timer.start();
    }

}
