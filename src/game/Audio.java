package game;


import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Audio {
    private Clip clip;

    private Audio(String son) {

        try {
            AudioInputStream audio = AudioSystem.getAudioInputStream(getClass().getResource(son));
            clip = AudioSystem.getClip();
            clip.open(audio);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void play() {
        clip.start();
    }

    public static void playSound(String son) {
        Audio s = new Audio(son);
        s.play();
    }
}
