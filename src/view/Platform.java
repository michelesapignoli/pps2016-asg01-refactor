package view;

import controller.PlatformController;
import game.Keyboard;

import java.awt.*;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;

import utils.Constants;
import utils.Res;
import utils.Utils;

public class Platform extends JPanel implements Observer {

    private Image imgBackground;
    private Image castle;
    private Image start;
    private Image imgFlag;
    private Image imgCastle;
    private JLabel score;

    /**
     * Swing class Platform
     */
    public Platform() {
        super();
        this.createViewObjects();

        this.setFocusable(true);
        this.requestFocusInWindow();
        this.addKeyListener(new Keyboard());
    }

    private void createViewObjects(){
        this.imgBackground = Utils.getImage(Res.IMG_BACKGROUND);
        this.castle = Utils.getImage(Res.IMG_CASTLE);
        this.start = Utils.getImage(Res.START_ICON);
        this.imgCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.imgFlag = Utils.getImage(Res.IMG_FLAG);

        this.score= new JLabel();
        this.score.setFont(new Font(Constants.SCORE_FONT, Font.PLAIN, Constants.SCORE_FONT_SIZE));
        this.score.setForeground(Color.WHITE);
        this.score.setText(Constants.SCORE);
        this.add(score);


    }

    /**
     * Method called by Swing. It draws the Platform with its components.
     * @param g Graphics
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        PlatformController.getInstance().paint(g, imgBackground, castle, start, imgFlag, imgCastle);
    }

    /**
     * Update of the Observer pattern
     * @param o score label
     * @param score score
     */
    @Override
    public void update(Observable o, Object score) {
        this.score.setText(Constants.SCORE + score.toString());
    }
}
